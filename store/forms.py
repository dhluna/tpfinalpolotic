from django import forms
from .models import Product

class ProductForm(forms.ModelForm):

    price = forms.DecimalField(initial=0.00)
    description = forms.CharField(label="Descripción",widget=forms.Textarea(attrs={"title":"Descripción detalla del producto", "rows":10, "cols":90}))

    class Meta:
        model = Product
        fields = ('name', 'image', 'description', 'category_id', 'price')