from django.shortcuts import get_object_or_404, render
from django.http.response import HttpResponseRedirect
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.urls import reverse
from .models import Category, Product
from .forms import ProductForm
from django.db.models.query_utils import Q
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

#from django.contrib.auth.mixins import LoginRequiredMixin
#from django.contrib.auth.mixins import PermissionRequiredMixin


# Create your views here.
#LoginRequiredMixin, 

#class MyView(LoginRequiredMixin, View):
#    login_url = '/login/'
#    redirect_field_name = 'redirect_to'

#PermissionRequiredMixin
#class MyView(PermissionRequiredMixin, View):
    #permission_required = 'polls.add_choice'
    # Or multiple of permissions:
    #permission_required = ('polls.view_choice', 'polls.change_choice')

class base(View):
    template_name = ""
    companyname = "JAGUARETE KAA S.A."
    bcontent = {"company_name": companyname,
                "copyright": "Copyright 2021",
                "ndeveloper": "por David Luna"
               }

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, self.bcontent)

class home(base):
    template_name = "store/home.html"
    base.bcontent.update({"products_top3": Product.objects.all()[:3],
                          "products_next": Product.objects.all()[3:10]})

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.bcontent)

class about(base):
    template_name = "store/about.html"
    bcontent = {"app_name": "OLEC - e-commerce",
                "app_ver": "v1.0",
                "app_desc": "Trabajo final - Portal e-commerce"
               }
    def get(self, request):
        return render(request, self.template_name, self.bcontent)

class product(base, DetailView):
    model = Product
    template_name = "store/product.html"
    context_object_name="product"
    
    bcontent = {"app_name": "OLEC - e-commerce",
                "app_ver": "v1.0",
                "app_desc": "Trabajo final - Portal e-commerce"
               }

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    

class categories(base, ListView):
    model = Category
    template_name = "store/categories.html"
    context_object_name="categories"

    def get_context_data(self, **kwargs):
        context = super(categories, self).get_context_data(**kwargs)
        context.update(self.bcontent)
        return context

class login(base):
    template_name = "store/login.html"

class search(base):
    template_name = "store/search.html"

    def get(self, request, *args, **kwargs):
        print("metodo get de search")
        s_category = self.kwargs['id_cate']
        if s_category:
            search_resu = Product.objects.filter(category_id=s_category)
        base.bcontent.update({"search_resu":search_resu})
        return render(request, self.template_name, self.bcontent)

    def post(self, request, *args, **kwargs):
        print("metodo post de search")
        s_name_desc = request.POST.get('s_name_desc')
        s_category =  request.POST.get('s_category')
        if s_name_desc:
            search_resu = Product.objects.filter(Q(name__contains=s_name_desc)|Q(description__contains=s_name_desc))
        elif s_category:
            search_resu = Product.objects.filter(category_id=s_category)
        print(search_resu)
        base.bcontent.update({"search_resu":search_resu})
        return render(request, self.template_name, self.bcontent)

class user_register(base):
    template_name = "store/user_register.html"

class product_new(CreateView):
    template_name = "store/product_new.html"
    form_class = ProductForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

class product_upd(UpdateView):
    template_name = "store/product_upd.html"
    form_class = ProductForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
    
    def form_valid(self, form):
        return super().form_valid(form)

    def get_object(self):
        id = self.kwargs.get("id")
        return get_object_or_404(Product, id=id)

    def get_success_url(self):
        id = self.kwargs.get("id")
        return reverse("product", kwargs={"pk":id})


class product_del(DeleteView):
    template_name = "store/product_del.html"
    form_class = ProductForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Product, id=id_)

    def get_success_url(self):
        return reverse("home")

class cart_add(base):
    template_name = "store/cart.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})

class cart(base):
    template_name = "store/cart.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})