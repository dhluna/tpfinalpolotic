from django.db import models
from django.contrib.auth.models import User
from django.db.models import Sum
from django.urls.base import reverse

# Create your models here.
class Category(models.Model):
    description = models.CharField(max_length=255, null=False, verbose_name="Descripción")

    def __str__(self):
        return f"{self.description}"

class Product(models.Model):
    name = models.CharField(max_length=255, null=False, verbose_name="Nombre")
    image = models.FileField(upload_to='img/', verbose_name="Imagen", null=True)
    description = models.TextField(verbose_name="Descripción")
    price= models.FloatField(verbose_name="Precio")
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="product_category", verbose_name="Categoria")

    def __str__(self):
        return f"{self.name} precio: {self.price}"

    def get_absolute_url(self):
        return reverse("product", kwargs={"pk":self.id})

class ShopCart(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    products = models.ManyToManyField(Product, blank=True, related_name="products", verbose_name="Productos")

    def __str__(self):
        return f"{self.user.username}"