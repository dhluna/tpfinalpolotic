from django.urls import path
from .views import *

urlpatterns = [
    path('', home.as_view(), name="home" ),
    path('store/', home.as_view(), name="home" ),
    path('home/', home.as_view(), name="home" ),
    path('about/', about.as_view(), name="about"),
    path('login/', login.as_view(), name="login"),
    path('product/<int:pk>/', product.as_view(), name="product"),
    path('search/', search.as_view(), name="search"),
    path('search/<int:id_cate>', search.as_view(), name="search"),
    path('categories/', categories.as_view(), name="categories"),
    path('product_new/', product_new.as_view(), name="product_new"),
    path('product_upd/<int:id>', product_upd.as_view(), name="product_upd"),
    path('product_del/<int:id>', product_del.as_view(), name="product_del"),
    path('cart_add/<int:pid>', cart_add.as_view(), name="cart_add"),
    path('cart', cart.as_view(), name="cart"),
]
